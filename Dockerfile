# Technically a no-op, but leaving here for clarification reasons
FROM scratch

# Yeah... i am assuming you already built it, deal with it
COPY ./bin/shadowserver /

EXPOSE 8080

CMD ["/shadowserver"]
