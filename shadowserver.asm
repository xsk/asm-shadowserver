; https://blog.rchapman.org/posts/Linux_System_Call_Table_for_x86_64/
; https://filippo.io/linux-syscall-table/
; strace ans man is your friend

%define SOCKET_QUEUE 1 ; listening socket backlog size

global  _main ; export symbol for entry point

; ReadOnly section, everything immutable goes here
section .rodata
    startMessage:   db  '0.0.0.0:8080', 0x0d, 0x0a, 0x00         ; Starting message + line feed + termination
    startMessageLen equ $ - startMessage                         ; Calculate automatically message size

    httpOK:
        db  "HTTP/1.1 200 OK", 0x0d, 0x0a, 0x0d, 0x0a, 0x00     ; HTTP 200 response + line feed + termination
    httpOKLen equ $ - httpOK                        ; Calculate automatically message size

; Variables with initial values go here
section .data
    socket          dq  0                           ; will contain socket pointer when created - 8 bytes
    connfd          dq  0                           ; will contain the connected socket fd - 8 bytes
    b_optval        db  1                           ; boolean 'true' pointer for socket options - 1 byte

    s_readbuff     equ     90                       ; size of read buffer - if not enough, will cause a connection reset.
                                                    ; - increase to your liking, the more you add the bigger the binary
    readText       TIMES   s_readbuff  db  ' '      ; initialize with empty text equal to the buffer size
    
; Variables without initial value go here
section .bss
    s_address      resq    2                        ; will contain the address and port information pointer

; Codebase goes here
section .text

    _main:
        ; Show start message  
        mov     rax, 1                   ; prep rax for sys_write
        mov     rdi, 1                   ; unsigned int fd: 1 = stdout
        mov     rsi, startMessage        ; const char __user * buf: rodata string
        mov     rdx, startMessageLen     ; size_t count: string size
        syscall

        ; Create a socket
        mov     rax, 41                  ; prep rax for sys_socket
        mov     rdi, 2                   ; int family: 2 = PF_INET
        mov     rsi, 1                   ; int type: 1 = SOCK_STREAM ( TCP )
        mov     rdx, 6                   ; int protocol: 6 = TCP
        syscall                     
        ; syscall always returns a pointer, on failure PTR_ERR macro will printk
        ; later on, if socket is not pointing to a valid address program will exit
        mov     [socket], rax            ; struct socket *sock: returned from syscall

        ; Set up the socket address as a local variable
        ; NOTE: We don't really care about stack management best practices, 
        ; as this is the only var we need to create
        push    dword 0x00000000         ; 0.0.0.0
        push    word  0x901f             ; 8080 ( Reverse order according to calling convention )
        push    word  2                  ; address family : 2 = PF_INET
        mov     [s_address], rsp         ; store pointer to address info
        add     rsp, 0xc                 ; point stack to before variable

        ; Set socket options
        mov     rax, 54                  ; prep rax for sys_setsockopt
        mov     rdi, [socket]            ; int fd: socket pointer
        mov     rsi, 1                   ; int level: SOL_SOCKET
        mov     rdx, 2                   ; int optname: SO_REUSEADDR
        mov     r10, b_optval            ; char *optval: set socket address
        mov     r8, 8                    ; int optlen: one byte
        syscall                         
        cmp     rax, 0                   ; exit 1 on error
        jne     exit1     

        ; Bind socket to address
        mov     rax, 49                  ; prep rax for sys_bind
        mov     rdi, [socket]            ; int fd: socket pointer
        mov     rsi, [s_address]         ; struct sockaddr __user * umyaddr: socket address pointer
        mov     rdx, 32                  ; int addrlen : 32 bit
        syscall                         
        cmp     rax, 0                   ; exit 1 on error 
        jne     exit1                               

        ; Listen  - Mark socket as passive and will be used to accept connections
        mov     rax, 50                  ; prep rax for sys_listen
        mov     rdi, [socket]            ; int fd: socket pointer
        ; defines the maximum length to which the queue of pending connections for sockfd may grow
        ; no need to be more than 1 here, but it is a good chance to show how %define works
        mov     rsi, SOCKET_QUEUE        ; int backlog: SOCKET_QUEUE
        syscall   

    listen_loop:
        ; Accept a new connection, blocking version - `man listen` to see what that means
        mov     rax, 43                  ; prep rax for sys_accept
        mov     rdi, [socket]            ; int fd: socket pointer
        xor     rsi, rsi                 ; struct sockaddr *upeer_sockaddr: 0 ( not set )
        xor     rdx, rdx                 ; int __user * upeer_addrlen: 0 ( not set )
        ; creates a new socket on a connected state , and returns a new file descriptor referring to that socket
        syscall
        cmp     rax, 0                   ; exit 1 on error 
        jle     exit1
        mov     [connfd], rax            ; Store connected socket file descriptor pointer

        ; Read from the connected socket
        ; TODO : If we don't read fully, we will return answer too fast and cause a connection reset from client side
        mov     rax, 0                   ; prep rax for sys_read
        mov     rdi, [connfd]            ; unsigned int fd : connected socket fd
        mov     rsi, readText            ; char __user * buf : 
        mov     rdx, s_readbuff          ; size_t count : 
        syscall                          ; Return bytes read rax

        ; debug - Write to stdout what we received to give some running feedback
        ; You can comment this syscall out if you want to be completely silent
        mov     rax, 1                   ; prep rax for sys_write
        mov     rdi, 1                   ; unsigned int fd: 1 = stdout
        mov     rsi, readText            ; const char __user * buf: text read from socket
        mov     rdx, s_readbuff          ; size_t count: read buffer size
        syscall

        ; Write the HTTP success response to the connected socket
        mov     rax, 1                   ; prep rax for sys_write
        mov     rdi, [connfd]            ; unsigned int fd: connected socket fd
        mov     rsi, httpOK              ; const char __user * buf: HTTP 200
        mov     rdx, httpOKLen           ; size_t count: string size
        syscall

        ; Close connected socket
        mov     rax, 3                   ; sys_close()
        mov     rdi, [connfd]            ; unsigned int fd : connected socket fd
        syscall

        ; Back to waiting for accepting for new connection
        jmp     listen_loop              


    ; Error termination flow
    exit1:   
        mov     rdi, 1                   ; set exit code to 1
        mov     rax, 60                  ; prep rax for exit
        syscall
 
