# ASM-SHADOWSERVER

## Simplest Web server, written in x86-64 Assembly

### What problems does this software solve ?

I needed a web server that can run in any Linux kernel machine, would be happy with any request, and can easily fit in a k8s configmap.
There are some valid reasons to do this, but in my case is mostly laziness.

Named it *shadowserver* as it is so small in both size and running footprint that the majority of Engineers born after 1990 would not fathom it being any kind of functioning web server, also, because it can be injected as a base64 text on any pod container, originating from the shadowy paths of a Deployment chart as much as a binary in concerned.

Technically you can assemble it for any Linux kernel ported arch that `NASM` supports, but you would have to configure the Makefile yourself to do so. ( Basic networking syscalls are required ).

### Usage

`Make run`

### Notes

- Server will listen on 0.0.0.0:8080
  - Most likely something else will also do the same thing and use that Port already, change it to fit your needs, Port definition is Little Endian.
- Everything received will return a 200 OK code with no response size set.
  - Technically OK for most needs, but not compliant with [HTTP Spec](https://www.rfc-editor.org/rfc/rfc9110#status.200)
- If the request is more than the hardcoded char buffer long, the client will trigger a connection reset, as the web server does not care for anything more and will reply back prematurely.
  - You can change that easily to fit your needs, increasing the size will increase `.data` section size.
  - Server will echo back on stdout every request made.
- Added a ton of comments so that others might use it as a learning opportunity, but since it is a very lazy implementation ( literally tcp4 textbook syscalls ), expect that you will have to deal with stack management if you expand the code.
- No security checks implied, not in code nor linker params, this Project optimizes for binary size.

### Required software 

- Linux
- [NASM](https://nasm.us/) ( or equivalent ) Assembler

### License

- [MIT](LICENSE)
