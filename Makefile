build-release:
	mkdir -p ./bin
	nasm -felf64 shadowserver.asm
	ld -z noseparate-code shadowserver.o -e _main -o bin/shadowserver
	rm shadowserver.o
	strip bin/shadowserver

build-debug:
	mkdir -p ./bin
	nasm -felf64 shadowserver.asm
	ld shadowserver.o -e _main -o bin/shadowserver-debug

run: build-release
	./bin/shadowserver

run-debug: build-debug
	./bin/shadowserver-debug

run-trace: build-debug
	strace ./bin/shadowserver-debug

clean:
	rm -v bin/shadowserver bin/shadowserver-debug shadowserver.o
